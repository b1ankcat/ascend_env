#! /bin/bash
cd $HOME
# install docker
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
echo "\n" | sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli
sudo systemctl start docker
sudo systemctl enable docker

# change docker mirror
sudo touch /etc/docker/daemon.json
echo "{" | sudo tee -a /etc/docker/daemon.json
echo "\"registry-mirrors\": [\"https://docker.1panel.live\", \"https://docker.ketches.cn\", \"https://hub.iyuu.cn\"]" | sudo tee -a /etc/docker/daemon.json
echo "}" | sudo tee -a /etc/docker/daemon.json
sudo systemctl restart docker

# create container
sudo docker pull blankcat/ascend_cann:latest
mkdir $HOME/project
sudo docker run -itd -v $HOME/project:/home/project -p 8080:8080 --name cann blankcat/ascend_cann:latest
sudo docker exec cann nohup code-server > /dev/null 2>&1 &
sudo docker exec cann git clone https://gitee.com/ascend/samples.git /home/project/samples



