#! /bin/bash
# 初始化必须数组
CANN_VERSION_LIST=($(curl --progress-bar -s -G -H "Referer: https://www.hiascend.com/developer/download/community/result?module=cann" \
    --data "category=0&offeringType&model&offeringList=CANN"  https://www.hiascend.com/ascendgateway/ascendservice/resourceDownload/mappingList |
    grep -oP '"versionName":\s*"\K[^"]+'))
SHORT_PARAMS_LIST=c:,a:,s:,r:,v:,h
LONG_PARAMS_LIST=cann:,snap:,source:,runner:,coder:,help
OPTS=$(getopt -a --options $SHORT_PARAMS_LIST --longoptions $LONG_PARAMS_LIST -- "$@")
eval set -- "$OPTS"

function help()
{
    echo " "
    echo "----------------------使用帮助--------------------------"
    echo "可输入的命令行参数和含义为: "
    echo "    -c || --cann : cann版本, 请输入版本号如8.0.RC3.alpha002"
    echo "    -a || --snap: 是否移除snap, 请输入y或n"
    echo "    -s || --source: 是否更换镜像源, 请输入y或n"
    echo "    -r || --runner: 是否安装gitlab-runner, 请输入y或n"
    echo "    -v || --coder: 是否安装code-server, 请输入y或n"
    echo "    -h || --help: 使用帮助"
    echo "--------------------------------------------------------"
    echo " "
}

function check_opts()
{
    if [ "$2" != "y" ] && [ "$2" != "n" ]; then
        echo "-$1 参数值必须是y或n"
        exit 1
    fi
}

function task_process()
{
    TOTAL=50
    BAR_WIDTH=50

    # 初始化进度条
    for ((i=0; i<=TOTAL; i++)); do
        # 计算进度百分比
        PERCENT=$((i * 100 / TOTAL))
        # 计算当前进度条长度
        FILLED_BAR=$((i * BAR_WIDTH / TOTAL))
        EMPTY_BAR=$((BAR_WIDTH - FILLED_BAR))
        # 显示进度条
        printf "\r[%-${BAR_WIDTH}s] %3d%%" "$(printf '#%.0s' $(seq 1 $FILLED_BAR))" "$PERCENT"
        # 模拟任务延迟
        sleep 0.1
    done
}

# 解析命令行参数
USE_OPTS="n"
while :
do
  case $1 in
    (-c | --cann )
        CANN_VERSION="$2"
        USE_OPTS="y"
        shift 2;;
    (-a | --snap)
        IS_REMOVE_SNAP="$2"
        check_opts a $2
        USE_OPTS="y"
        shift 2;;
    (-s | --source)
        IS_UPDATE_SOURCE="$2"
        check_opts s $2
        USE_OPTS="y"
        shift 2;;
    (-r | --runner)
        IS_RUNNER="$2"
        check_opts r $2
        USE_OPTS="y"
        shift 2;;
    (-v | --coder)
        IS_CODER="$2"
        check_opts v $2
        USE_OPTS="y"
        shift 2;;
    (-h | --help)
        help
        exit 1
        shift 2;;
    (--)
        shift
        break
        ;;
    (*)
        help
        exit 1 
        ;;
  esac
done

# 如果使用了命令行参数，则检查所有参数的正确性
if [ "$USE_OPTS" == "y" ]; then
    # 获取最新cann版本
    if [ "$CANN_VERSION" == "latest" ]; then
        CANN_VERSION=${CANN_VERSION_LIST[0]}
    fi
    # 检查CANN_VERSION是否在LIST中
    CANN_VERSION_FOUND="n"
    for var in ${CANN_VERSION_LIST[@]}
    do
        if [[ "$CANN_VERSION" == "$var" ]]; then
            CANN_VERSION_FOUND="y"
            break
        fi
    done
    if [ "$CANN_VERSION_FOUND" == "n" ]; then
        echo "cann版本错误, 当前为 : $CANN_VERSION"
        echo "本软件支持选择任意版本的cann"
        exit 1
    fi
    # 检查是否缺少参数
    if [ -z "$CANN_VERSION" ] || [ -z "$IS_REMOVE_SNAP" ] || \
    [ -z "$IS_UPDATE_SOURCE" ] || [ -z "$IS_RUNNER" ] || [ -z "$IS_CODER" ]; then
        echo "当前缺少参数或参数错误，请查看帮助并使用正确的参数运行" >&2
        help
        exit 1
    fi
    echo "--------------------------------------------------------"
    echo "1.当前选择的CANN版本为: $CANN_VERSION"
    echo "2.当前选择的是否更新Ubuntu源为: $IS_UPDATE_SOURCE"
    echo "3.当前选择的是否删除snap为: $IS_REMOVE_SNAP"
    echo "4.当前选择的是否安装gitlab-runner为: $IS_RUNNER"
    echo "5.当前选择的是否安装code-server为: $IS_CODER"
    echo "启动安装脚本......"
    echo "--------------------------------------------------------"
    task_process
else
    FLOW_TYPE=0
    IS_UPDATE_SOURCE="y"
    IS_REMOVE_SNAP="y"
    CANN_NUM=0
    CANN_VERSION=${CANN_VERSION_LIST[0]}
fi
CANN_ADDR=$(curl --progress-bar -s -G -H "Referer: https://www.hiascend.com/developer/download/community/result?module=cann" \
    --data "versionName=${CANN_VERSION}"  https://www.hiascend.com/ascendgateway/ascendservice/cann/info/zh/0 |
    grep -oP '"downloadUrl":"\K[^"]*toolkit[^"]*x86[^"]*run[^"]*' | head -n 1 | sed 's/ /%20/g')

function change_cann_version_and_addr()
{
    clear
    echo "                                                        "
    echo "输入下列序号更改cann版本"
    echo "--------------------------------------------------------"
    index=0
    for var in ${CANN_VERSION_LIST[@]}
    do
        echo "$index. $var"
        ((index++))
    done
    echo "--------------------------------------------------------"
    echo "输入的序号必须大于等于0且小于${#CANN_VERSION_LIST[@]}:"
    read CANN_NUM
    while (( $CANN_NUM < 0 || $CANN_NUM >= ${#CANN_VERSION_LIST[@]} ))
    do
        echo "输入的序号必须大于等于0且小于${#CANN_VERSION_LIST[@]}:"
        read CANN_NUM
    done
    CANN_VERSION=${CANN_VERSION_LIST[CANN_NUM]}
    CANN_ADDR=$(curl --progress-bar -s -G -H "Referer: https://www.hiascend.com/developer/download/community/result?module=cann" \
        --data "versionName=${CANN_VERSION}"  https://www.hiascend.com/ascendgateway/ascendservice/cann/info/zh/0 |
        grep -oP '"downloadUrl":"\K[^"]*toolkit[^"]*x86[^"]*run[^"]*' | head -n 1 | sed 's/ /%20/g')
    return
}

function change_update_source()
{
    clear
    echo "                                                        "
    echo "输入下列序号更改是否更新Ubuntu源"
    echo "--------------------------------------------------------"
    echo "n.不更新Ubuntu源"
    echo "y.更新Ubuntu源"
    echo "--------------------------------------------------------"
    echo "输入的序号必须为y或n:"
    read IS_UPDATE_SOURCE
    while true
    do
        if [ "$IS_UPDATE_SOURCE" == "y" ] || [ "$IS_UPDATE_SOURCE" == "n" ]; then
            return
        fi
        echo "输入的序号必须为y或n:"
        read IS_UPDATE_SOURCE
    done
    return
}

function change_remove_snap()
{
    clear
    echo "                                                        "
    echo "输入下列序号更改是否删除snap"
    echo "--------------------------------------------------------"
    echo "n.不删除snap"
    echo "y.删除snap"
    echo "--------------------------------------------------------"
    echo "输入的序号必须为y或n:"
    read IS_REMOVE_SNAP
    while true
    do
        if [ "$IS_REMOVE_SNAP" == "y" ] || [ "$IS_REMOVE_SNAP" == "n" ]; then
            return
        fi
        echo "输入的序号必须为y或n:"
        read IS_REMOVE_SNAP
    done
    return
}

function gui()
{
    clear
    echo "                                                        "
    echo "本软件当前仅适配ubuntu22.04 LTS和自带的python3.10         "
    echo "输入下列序号更改安装选项"
    echo "--------------------------------------------------------"
    echo "1.更改CANN版本        [当前的选择为$CANN_VERSION]"
    echo "2.是否更新Ubuntu源    [当前的选择为$IS_UPDATE_SOURCE]"
    echo "3.是否删除snap        [当前的选择为$IS_REMOVE_SNAP]"
    echo "4.开始安装"
    echo "--------------------------------------------------------"
    echo "输入的序号必须大于等于1且小于等于4:"
    read FLOW_TYPE
    while (( $FLOW_TYPE < 1 || $FLOW_TYPE > 4 ))
    do
        echo "输入的序号必须大于等于1且小于等于4:"
        read FLOW_TYPE
    done
}

function gui_main()
{
    while true
    do
        gui
        if (( $FLOW_TYPE == 1 ))
        then
            change_cann_version_and_addr
        elif (( $FLOW_TYPE == 2 ))
        then
            change_update_source
        elif (( $FLOW_TYPE == 3 ))
        then
            change_remove_snap
        elif (( $FLOW_TYPE == 4 ))
        then
            break
        fi
    done
}

# 如果使用了命令行参数，则不再显示GUI
if [ "$USE_OPTS" == "y" ]; then
  echo "开始执行安装命令"
else
  gui_main
fi

function update_sources()
{
    if test -e "/etc/apt/sources.list.back";then 
        echo "已存在备份sources.list.back"
    else
        cp /etc/apt/sources.list /etc/apt/sources.list.back
        echo "将sources.list备份至sources.list.back"
    fi

    rm /etc/apt/sources.list
    touch /etc/apt/sources.list
    source /etc/os-release
    echo "deb https://mirrors.huaweicloud.com/ubuntu/ ${VERSION_CODENAME} main restricted universe multiverse" >> /etc/apt/sources.list
    echo "deb https://mirrors.huaweicloud.com/ubuntu/ ${VERSION_CODENAME}-updates main restricted universe multiverse" >> /etc/apt/sources.list
    echo "deb https://mirrors.huaweicloud.com/ubuntu/ ${VERSION_CODENAME}-backports main restricted universe multiverse" >> /etc/apt/sources.list
    echo "deb https://mirrors.huaweicloud.com/ubuntu/ ${VERSION_CODENAME}-security main restricted universe multiverse" >> /etc/apt/sources.list
    echo "deb https://mirrors.huaweicloud.com/ubuntu/ ${VERSION_CODENAME}-proposed main restricted universe multiverse" >> /etc/apt/sources.list

    echo "sources.list更新完成"
}

function remove_snap()
{
    systemctl disable snapd.service
    systemctl disable snapd.socket
    systemctl disable snapd.seeded.service
    snap remove firefox
    snap remove snap-store
    snap remove gtk-common-themes
    snap remove gnome-3-38-2004
    snap remove core18
    snap remove snapd-desktop-integration
    rm -rf /var/cache/snapd/
    apt autoremove -y --purge snapd
    rm -rf ~/snap
    
    rm /etc/apt/preferences.d/firefox-no-snap
    touch /etc/apt/preferences.d/firefox-no-snap
    echo "Package: firefox*" >> /etc/apt/preferences.d/firefox-no-snap
    echo "Pin: release o=Ubuntu*" >> /etc/apt/preferences.d/firefox-no-snap
    echo "Pin-Priority: -1" >> /etc/apt/preferences.d/firefox-no-snap
    
    echo "snap移除完成"
}

function install_cann_depend()
{
    cd /home
    apt-get update
    apt-get install -y --no-install-recommends vim curl sudo lcov wget ca-certificates
    apt-get install -y --no-install-recommends build-essential libncurses5-dev libncursesw5-dev libreadline-dev  libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libnss3-dev libedit-dev wget
    apt-get install -y --no-install-recommends gcc g++ make cmake zlib1g zlib1g-dev openssl libsqlite3-dev libssl-dev libffi-dev unzip pciutils net-tools libblas-dev gfortran libblas3 zip unzip
    if ! command -v python3 &> /dev/null
    then
        apt-get install -y --no-install-recommends python3
    fi

    apt-get install -y --no-install-recommends python3-pip
    mkdir -p ~/.pip 
    cd ~/.pip
    sudo echo "[global]" >>  pip.conf
    sudo echo "index-url = https://mirrors.huaweicloud.com/repository/pypi/simple" >>  pip.conf
    sudo echo "trusted-host = mirrors.huaweicloud.com" >>  pip.conf
    sudo echo "timeout = 120" >>  pip.conf
    pip3 install attrs cython numpy==1.24 decorator sympy cffi pyyaml pathlib2 psutil protobuf==3.20 scipy requests absl-py
    
    echo "cann依赖部署完成"
}   

function install_cann()
{
    cd /home
    wget -q "$CANN_ADDR"
    if [[ "$CANN_VERSION" == *"beta"* ]]; then
        CANN_VERSION="${CANN_VERSION%.beta*}"
    fi
    chmod +x Ascend-cann-toolkit_${CANN_VERSION}_linux-x86_64.run
    ./Ascend-cann-toolkit_${CANN_VERSION}_linux-x86_64.run --install --quiet
    sudo echo "export LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/x86_64-linux/devlib/:\$LD_LIBRARY_PATH" >>  ~/.bashrc
    sudo echo "source /usr/local/Ascend/ascend-toolkit/set_env.sh" >>  ~/.bashrc
    sudo echo "source /usr/local/Ascend/ascend-toolkit/latest/bin/setenv.bash" >>  ~/.bashrc
    source ~/.bashrc

    rm -rf /home/Ascend-cann-toolkit_${CANN_VERSION}_linux-x86_64.run
    echo -e "cann安装完成"
}

function install_runner()
{
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
    apt-get install -y --no-install-recommends gitlab-runner
    echo -e "gitlab-runner安装完成"
}

function install_coder()
{
    cd /home
    wget -q "https://gitee.com/b1ankcat/file-repo/releases/download/coder/coder.deb"
    dpkg -i coder.deb
    rm -rf ~/.config/code-server/config.yaml
    mkdir -p ~/.config
    mkdir -p ~/.config/code-server
    touch ~/.config/code-server/config.yaml
    sudo echo "bind-addr: 0.0.0.0:8080" >> ~/.config/code-server/config.yaml
    sudo echo "auth: password" >> ~/.config/code-server/config.yaml
    sudo echo "password: mycoder" >> ~/.config/code-server/config.yaml
    sudo echo "cert: false" >> ~/.config/code-server/config.yaml

    rm -rf /home/coder.deb
    echo -e "code-server安装完成"
}


if [ "$IS_UPDATE_SOURCE" == "y" ]; then
    update_sources
fi
if [ "$IS_REMOVE_SNAP" == "y" ]; then
    remove_snap
fi
install_cann_depend
install_cann
if [ "$IS_RUNNER" == "y" ]; then
    install_runner
fi
if [ "$IS_CODER" == "y" ]; then
    install_coder
fi

apt-get clean
rm -rf /var/lib/apt/lists/*