# 一、root用户的一键安装CANN开发环境脚本
[一键安装脚本(gitee地址)](https://gitee.com/b1ankcat/ascend_env/blob/master/install_cann.sh)

下载后直接运行:
```bash
wget https://gitee.com/b1ankcat/ascend_env/raw/master/install_cann.sh
bash install_cann.sh
```
同时在交互式环境选择选项，最后一键安装即可

***注意：本脚本对安装后的一切情况皆不负责，建议在虚拟机中运行防止误删文件，同时更建议直接使用docker环境，可以直接免安装使用cann环境***

# 二、基于ubuntu22.04、python3.10和华为官方cann-toolkit包的docker环境

[docker仓库(gitee地址)](https://gitee.com/b1ankcat/ascend_env)

[docker Hub](https://hub.docker.com/r/blankcat/ascend_cann)


省略安装docker的过程，同时已假设您具有docker的基本操作能力

## 1.运行cann开发环境

0. 获取镜像

```bash
docker pull blankcat/ascend_cann:latest
```

注意：latest会紧跟官方最新版本，当遇到beta版本时会分离出beta版本留存

当前的latest版本：8.0.0.alpha003

1. 启动镜像

```bash
docker run -itd -v /home/project:/home/project -p 36080:8080 --name cann blankcat/ascend_cann:latest
```

该命令将容器中的/home/project文件夹与宿主环境中的/home/project文件夹绑定，可以直接访问宿主环境中/home/project的文件夹内容

36080为code-server端口

2. 进入CLI环境

```bash
docker exec -it cann /bin/bash
```

可以在CLI环境直接运行程序
