FROM ubuntu:22.04
ENV TZ=Asia/Shanghai
LABEL maintainer="blankcat"
USER root
WORKDIR /home

# 安装依赖
RUN apt-get update && \
 apt-get install -y --no-install-recommends ca-certificates wget curl && \
 wget https://gitee.com/b1ankcat/ascend_env/raw/master/install_cann.sh && \
 chmod +x install_cann.sh && \
 bash install_cann.sh -c latest -p python3.9.1 -a y -s y -r y -v y && \
 rm -rf /home/install_cann.sh

WORKDIR /
VOLUME /home/project