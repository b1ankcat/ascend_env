#! /bin/bash
container_exists=$(sudo docker ps -a --filter "name=cann" --format '{{.Names}}')

if [ -z "$container_exists" ]; then
    sudo docker pull blankcat/ascend_cann:latest
    sudo docker run -itd -v $HOME/project:/home/project -p 8080:8080 --name cann blankcat/ascend_cann:latest
else
    sudo docker start cann
fi

sudo docker exec cann nohup code-server > /dev/null 2>&1 &